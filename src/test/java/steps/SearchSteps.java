package steps;

import io.cucumber.java.en.When;
import pages.BasePage;
import pages.Page;

public class SearchSteps extends Page {

    @When("^they search for \"([^\"]*)\"$")
    public void theySearchFor(String searchTerm) {
        instanceOf(BasePage.class).searchFor(searchTerm);
    }
}

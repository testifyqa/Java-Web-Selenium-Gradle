package steps;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import pages.Page;
import pages.RedditPage;
import pages.SearchResultsPage;

public class RedditSteps extends Page {

    @And("^they view the first result$")
    public void theyViewTheFirstResult() {
        instanceOf(SearchResultsPage.class).viewFirstResult();
    }

    @Then("^they see the Reddit homepage$")
    public void theySeeTheRedditHomepage() {
        instanceOf(RedditPage.class).assertIAmOnRedditPage();
    }
}
